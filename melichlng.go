package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sort"
	"strconv"
	"strings"
)

type user struct {
	Name    string
	CantMov int
	Avg     float64
}

type movementAvg struct {
	CantMov     int
	Type        string
	Avg         float64
	MaxMovsUser string
	Users       map[string]user
}

type result struct {
	TypeMov     string
	Avg         float64
	MaxMovsUser user
	Percentil95 float64
}

const (
	namePos   int = 0
	typePos   int = 1
	amountPos int = 2
)

func (u user) addMovement(amount float64) user {
	u.CantMov++
	cantoMovF64 := float64(u.CantMov)
	if u.CantMov == 2 {
		u.Avg = (u.Avg + amount) / cantoMovF64
	} else {
		u.Avg = (((u.Avg)*cantoMovF64 - 1) + amount) / cantoMovF64
	}
	return u
}

func (mAvg movementAvg) addMovAvg(amount float64, uName string) movementAvg {
	// Add the amount of the movement to the user or add the user if it does not exist
	u, uExist := mAvg.Users[uName]
	if uExist {
		u = u.addMovement(amount)
	} else {
		u = user{CantMov: 1, Name: uName, Avg: amount}
	}
	mAvg.Users[uName] = u

	// Verify that the user has the greatest number of movements
	if mAvg.MaxMovsUser == "" {
		mAvg.MaxMovsUser = u.Name
	} else {
		if u.CantMov > mAvg.Users[mAvg.MaxMovsUser].CantMov {
			mAvg.MaxMovsUser = u.Name
		}
	}

	// Calculate the average
	mAvg.CantMov++
	cantMovF64 := float64(mAvg.CantMov)
	if mAvg.CantMov == 1 {
		mAvg.Avg = amount
	} else if mAvg.CantMov == 2 {
		mAvg.Avg = (mAvg.Avg + amount) / cantMovF64
	} else {
		mAvg.Avg = (((mAvg.Avg)*cantMovF64 - float64(1)) + amount) / cantMovF64
		fmt.Printf("Debug: [Dividendo: %f] \n", (((mAvg.Avg)*cantMovF64 - float64(1)) + amount))
		fmt.Printf("Debug: [Divisor: %f] \n", cantMovF64)
	}
	fmt.Printf("Debug: [Avg: %f] \n", mAvg.Avg)
	return mAvg
}

func (mAvg movementAvg) debugIt() {
	fmt.Printf("Debug: [Type: %s, CantMov: %d, Avg: %f, MaxMovsUser: %s]\n", mAvg.Type, mAvg.CantMov, mAvg.Avg, mAvg.MaxMovsUser)
}

func (r result) toString() string {
	return fmt.Sprintf("[Type: %s] [Avg: %f] [Max Movement User: %s - %d movs.] [Percentil95: %f]", r.TypeMov, r.Avg, r.MaxMovsUser.Name, r.MaxMovsUser.CantMov, r.Percentil95)
}

func toArray(line []byte) []string {
	//fmt.Println(string(line))
	aux := strings.Split(string(line), "]")
	if len(aux) == 4 {
		result := []string{
			strings.Split(aux[namePos], ":")[1],
			strings.Split(aux[typePos], ":")[1],
			strings.Split(aux[amountPos], ":")[1]}
		return result
	} else {
		return nil
	}
}

func resolveLine(line []byte, movByTypePerUser map[string]movementAvg, movByType map[string][]float64) {
	movement := toArray(line)
	if movement != nil {
		typeMov := movement[typePos]
		movAvg, movAvgExist := movByTypePerUser[typeMov]
		if !movAvgExist {
			movAvg = movementAvg{CantMov: 0, Avg: 0, Type: typeMov, Users: make(map[string]user), MaxMovsUser: ""}
		}
		amount, err := strconv.ParseFloat(movement[amountPos], 64)
		check(err)
		movAvg = movAvg.addMovAvg(amount, movement[namePos])
		movByTypePerUser[typeMov] = movAvg
		movByType[typeMov] = append(movByType[typeMov], amount)
	}

}

func generateResults(movByTypePerUser map[string]movementAvg, movByType map[string][]float64) map[string]result {
	results := make(map[string]result)

	for key, movs := range movByType {
		movAvg := movByTypePerUser[key]

		// Percentil 95
		sort.Float64s(movs)
		movSize := float64(len(movs))
		per95 := movs[int(movSize*0.95)]

		results[key] = result{Percentil95: per95, Avg: movAvg.Avg, MaxMovsUser: movAvg.Users[movAvg.MaxMovsUser], TypeMov: key}
	}
	return results
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	movByType := make(map[string][]float64)
	movByTypePerUser := make(map[string]movementAvg)
	path := os.Args[1]
	resPath := os.Args[2]
	rFile, err := os.Open(path)
	check(err)
	defer rFile.Close()
	reader := bufio.NewReader(rFile)

	for {
		line, isPrefix, err := reader.ReadLine()
		if err == io.EOF {
			break
		} else {
			check(err)
		}
		if !isPrefix {
			resolveLine(line, movByTypePerUser, movByType)
		}
	}
	wFile, err := os.Create(resPath)
	check(err)
	defer rFile.Close()
	writer := bufio.NewWriter(wFile)

	results := generateResults(movByTypePerUser, movByType)
	for _, res := range results {
		writer.WriteString(fmt.Sprintf("%s \n", res.toString()))
		fmt.Printf("%s \n", res.toString())
	}
	writer.Flush()
}
