## Meli Golang Workshop Challenge 

- Cuando se ejecute el proceso, se deben pasar dos parámetros. El primero representa la ruta donde se encuentra el archivo que se va a procesar y el segundo representa la ruta dónde se escribirá el resultado del proceso.
- El comando debe verse parecido al siguiente: ```bash~$ ./melichlng './movements.log' './result.log' ```